FROM java:8
WORKDIR /
ADD target/jeeurekaserver-*.jar jeeurekaserver.jar
EXPOSE 8761 8080
CMD ["java", "-jar", "jeeurekaserver.jar"]